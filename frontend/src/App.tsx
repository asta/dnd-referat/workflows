import React from "react"
import {ChakraProvider, Box, Flex, HStack} from "@chakra-ui/react"
import theme from "./theme.js"
import "@fontsource/fredoka-one"
import {FormBody} from "./components/formBody";
import {ToolBar} from "./components/tool-bar";
import {Title} from "./components/title";
import {Logo} from "./components/logo";
import * as cons from "./constants"

function App() {
    return (
        <ChakraProvider theme={theme}>
            <Flex boxShadow={cons.shd_white} bg={cons.grdnt_purple} m={cons.m} borderRadius="xl">
                <Box textAlign="center" width="100%" fontSize="xl" borderRadius="xl">
                    <HStack spacing={20} m={cons.m}>
                        <Logo/>
                        <ToolBar/>
                        <Title/>
                    </HStack>
                    <FormBody/>
                </Box>
            </Flex>
        </ChakraProvider>
    )
}

export default App;