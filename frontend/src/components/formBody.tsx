import {Alert, AlertDescription, AlertIcon, AlertTitle, VStack} from "@chakra-ui/react";
import {Formio} from "formiojs";
import * as cons from "../constants";

window.onload = function () {
    Formio.createForm(document.getElementById('formio'), 'https://examples.form.io/example')
};

function Info() {
    return (
        <Alert status="info" variant="subtle" flexDirection="column" alignItems="center" justifyContent="center"
               textAlign="center" borderRadius="xl" width="50%" m={cons.m}>
            <AlertIcon boxSize="40px"/>
            <AlertTitle m={cons.m} fontSize="lg">
                🚧 This Site is still under construction! 🚧
            </AlertTitle>
            <AlertDescription maxWidth="sm">
                Thanks for for testing..
            </AlertDescription>
        </Alert>
    )
}

function FormBody() {
    return (
        <VStack m={cons.m} borderRadius="xl" bg={cons.grdnt_white} boxShadow={cons.shd_in_white}>
            <Info/>
            <div style={{fontFamily: "Open Sans", padding: "50px", fontWeight: 600}} id={"formio"}/>
        </VStack>
    )
}

export {FormBody}