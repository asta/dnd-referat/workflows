import {Center} from "@chakra-ui/react";
import * as cons from "../constants";

function Title() {
    return (
        <Center width={"100%"} height={150} borderRadius="xl" bg={cons.grdnt_white} boxShadow={cons.shd_in_white}>
            <span style={{fontSize: 42, fontFamily: "Fredoka One"}}>
                Form Automatisierung
            </span>
        </Center>
    )
}

export {Title}