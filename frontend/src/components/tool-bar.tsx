import {Center, CircularProgress, CircularProgressLabel, Tooltip} from "@chakra-ui/react";
import * as cons from "../constants";

function ToolBar() {
    return (
        <Center borderRadius={"xl"}>
            <CircularProgress bg={cons.grdnt_purple} boxShadow={cons.shd_purple} borderRadius={"50%"} capIsRound={true}
                              value={40} size="120px" trackColor={"#a71868"} color={"#fcfaea"} thickness="5px">
                <Tooltip label="Completion progress 🌼" aria-label="A tooltip" colorSchema={"telegram"}>
                    <CircularProgressLabel style={{fontFamily: "Fredoka One"}} color={"#fcfaea"}>
                        40%
                    </CircularProgressLabel>
                </Tooltip>
            </CircularProgress>
        </Center>
    )
}

export {ToolBar}