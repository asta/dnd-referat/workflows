import {Center, Image} from "@chakra-ui/react";
import * as cons from "../constants"


function Logo() {
    return (
        <Center bg={cons.grdnt_white} boxShadow={cons.shd_in_white} width={400} borderRadius="xl">
            <Image padding={"5%"} alt={"Logo: Asta Uni Goettingen."} src={"public/asta_logo_rbg.png"}
                fallbackSrc={"https://asta.uni-goettingen.de/wp-content/uploads/2017/03/asta_logo_rgb.png"}>
            </Image>
        </Center>
    )
}

export {Logo}