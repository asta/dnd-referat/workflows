import { screen } from "@testing-library/react"
import { render } from "./test-utils"
import App from "./App"

const React = require('react');
const ReactDOM = require('react-dom');

//TODO write axe tests
if (process.env.NODE_ENV !== 'production') {
  const axe = require('@axe-core/react');
  axe(React, ReactDOM, 1000);
}

test('renders asta logo with alt', () => {
  render(<App />);
  const linkElement = screen.getByAltText("Logo: Asta Uni Goettingen.");
  expect(linkElement).toBeInTheDocument();
});

