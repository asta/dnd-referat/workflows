export const shd_purple = "5px 5px 15px #610e3c, -5px -5px 15px #ed2294"
export const shd_white = "5px 5px 5px #9f9e93, -5px -5px 5px #ffffff"
export const shd_in_white = "inset 15px 15px 25px #a6a59a, inset -15px -15px 25px #ffffff"

export const grdnt_white = "linear-gradient(145deg, #fffffa, #e3e1d3)"
export const grdnt_purple = "linear-gradient(145deg, #96165e, #b31a6f)"

//margin
export const m = 7