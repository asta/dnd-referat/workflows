import { extendTheme } from "@chakra-ui/react"

const config = {
    initialColorMode: "light",
    useSystemColorMode: true,
}

const theme = extendTheme({styles: {
        fonts: {
            body: "Fredoka One",
        },
        global: {
            body: {
                bg: "#fcfaea",
                color: "gray:400",
            }
        }
    }, config })
export default theme