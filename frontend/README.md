### How to Docker

`sudo docker build -t auto-form-fe .`

`sudo docker run -it --rm -v ${PWD}:/app -v /app/node_modules -p 3001:3000 -e CHOKIDAR_USEPOLLING=true auto-form-fe`

This project was bootstrapped with
[Create React App](https://github.com/facebook/create-react-app).

## Learn More

You can learn more in the
[Create React App documentation](https://facebook.github.io/create-react-app/docs/getting-started).

To learn React, check out the [React documentation](https://reactjs.org/).
