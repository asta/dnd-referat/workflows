create table workflow
(
	id int not null
		constraint workflow_pk
			primary key,
	name text not null,
	forms json not null
);

create unique index workflow_name_uindex
	on workflow (name);

