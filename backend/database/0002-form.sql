create table form
(
	id int not null
		constraint form_pk
			primary key,
	name text not null
)
without rowid;

create unique index form_name_uindex
	on form (name);

