create table submission
(
	id int not null
		constraint submission_pk
			primary key,
	name text not null,
	formdata json not null,
	form int not null
		references form
			on update restrict on delete restrict
);

create unique index submission_name_uindex
	on submission (name);

