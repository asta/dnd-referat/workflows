# coding: utf-8

from __future__ import absolute_import

from flask import json
from six import BytesIO

from swagger_server.models.workflow import Workflow  # noqa: E501
from swagger_server.test import BaseTestCase


class TestWorkflowController(BaseTestCase):
    """WorkflowController integration test stubs"""

    def test_add_workflow(self):
        """Test case for add_workflow

        Inserts a new workflow
        """
        workflow_data = Workflow()
        response = self.client.open(
            '/v2/workflows',
            method='POST',
            data=json.dumps(workflow_data),
            content_type='application/json')
        self.assert200(response,
                       'Response body is : ' + response.data.decode('utf-8'))

    def test_delete_workflow(self):
        """Test case for delete_workflow

        Delete a workflow from the database
        """
        response = self.client.open(
            '/v2/workflows/{workflowId}'.format(workflowId=789),
            method='DELETE')
        self.assert200(response,
                       'Response body is : ' + response.data.decode('utf-8'))

    def test_get_workflow(self):
        """Test case for get_workflow

        Retrieves a workflow from the database
        """
        response = self.client.open(
            '/v2/workflows/{workflowId}'.format(workflowId=789),
            method='GET')
        self.assert200(response,
                       'Response body is : ' + response.data.decode('utf-8'))

    def test_get_workflows(self):
        """Test case for get_workflows

        List all workflows in the system
        """
        response = self.client.open(
            '/v2/workflows',
            method='GET')
        self.assert200(response,
                       'Response body is : ' + response.data.decode('utf-8'))


if __name__ == '__main__':
    import unittest
    unittest.main()
