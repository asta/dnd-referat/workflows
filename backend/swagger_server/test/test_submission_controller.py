# coding: utf-8

from __future__ import absolute_import

from flask import json
from six import BytesIO

from swagger_server.models.submission import Submission  # noqa: E501
from swagger_server.test import BaseTestCase


class TestSubmissionController(BaseTestCase):
    """SubmissionController integration test stubs"""

    def test_add_submission(self):
        """Test case for add_submission

        Add submission
        """
        submission_data = Submission()
        response = self.client.open(
            '/v2/workflows/{workflowId}/forms/{formId}/submissions'.format(workflowId=789, formId=789),
            method='POST',
            data=json.dumps(submission_data),
            content_type='application/json')
        self.assert200(response,
                       'Response body is : ' + response.data.decode('utf-8'))

    def test_delete_submission(self):
        """Test case for delete_submission

        Delete submission
        """
        response = self.client.open(
            '/v2/workflows/{workflowId}/forms/{formId}/submissions/{submissionId}'.format(workflowId=789, formId=789, submissionId=789),
            method='DELETE')
        self.assert200(response,
                       'Response body is : ' + response.data.decode('utf-8'))

    def test_get_submission(self):
        """Test case for get_submission

        Get submissions
        """
        response = self.client.open(
            '/v2/workflows/{workflowId}/forms/{formId}/submissions/{submissionId}'.format(workflowId=789, formId=789, submissionId=789),
            method='GET')
        self.assert200(response,
                       'Response body is : ' + response.data.decode('utf-8'))

    def test_get_submissions(self):
        """Test case for get_submissions

        Get all submissions for a single form
        """
        response = self.client.open(
            '/v2/workflows/{workflowId}/forms/{formId}/submissions'.format(workflowId=789, formId=789),
            method='GET')
        self.assert200(response,
                       'Response body is : ' + response.data.decode('utf-8'))


if __name__ == '__main__':
    import unittest
    unittest.main()
