# coding: utf-8

from __future__ import absolute_import

from flask import json
from six import BytesIO

from swagger_server.models.form import Form  # noqa: E501
from swagger_server.test import BaseTestCase


class TestFormController(BaseTestCase):
    """FormController integration test stubs"""

    def test_add_form(self):
        """Test case for add_form

        Insert a new form
        """
        form_data = Form()
        response = self.client.open(
            '/v2/workflows/{workflowId}/forms'.format(workflowId=789),
            method='POST',
            data=json.dumps(form_data),
            content_type='application/json')
        self.assert200(response,
                       'Response body is : ' + response.data.decode('utf-8'))

    def test_delete_form(self):
        """Test case for delete_form

        Delete a form
        """
        response = self.client.open(
            '/v2/workflows/{workflowId}/forms/{formId}'.format(workflowId=789, formId=789),
            method='DELETE')
        self.assert200(response,
                       'Response body is : ' + response.data.decode('utf-8'))

    def test_get_form(self):
        """Test case for get_form

        Get a specific form from a workflow
        """
        response = self.client.open(
            '/v2/workflows/{workflowId}/forms/{formId}'.format(workflowId=789, formId=789),
            method='GET')
        self.assert200(response,
                       'Response body is : ' + response.data.decode('utf-8'))

    def test_get_forms(self):
        """Test case for get_forms

        Get all forms to a workflow
        """
        response = self.client.open(
            '/v2/workflows/{workflowId}/forms'.format(workflowId=789),
            method='GET')
        self.assert200(response,
                       'Response body is : ' + response.data.decode('utf-8'))


if __name__ == '__main__':
    import unittest
    unittest.main()
