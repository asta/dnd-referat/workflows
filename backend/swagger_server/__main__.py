#!/usr/bin/env python3
import os

import connexion

from swagger_server import encoder


def main():
    port = int(os.getenv('FLASK_RUN_PORT', 8080))
    ip = os.getenv('FLASK_RUN_HOST', '0.0.0.0')
    debug = bool(os.getenv('FLASK_RUN_DEBUG', True))

    app = connexion.App(__name__, specification_dir='./swagger/')
    app.app.json_encoder = encoder.JSONEncoder
    app.add_api('swagger.yaml', arguments={'title': 'Formulars'})
    app.run(port=port, host=ip, debug=debug)


if __name__ == '__main__':
    main()
