import sqlite3
import urllib.parse

import connexion
from connexion import NoContent
from pymongo import MongoClient

from swagger_server.models.workflow import Workflow  # noqa: E501

DB_PATH = 'database/db.sqlite'


def add_workflow(workflow_data):  # noqa: E501
    """Inserts a new workflow

     # noqa: E501

    :param workflow_data: Workflow that shall be added to the database
    :type workflow_data: dict | bytes

    :rtype: None
    """
    if connexion.request.is_json:
        workflow_data = Workflow.from_dict(connexion.request.get_json())  # noqa: E501
    else:
        return NoContent, 405
    client = MongoClient("mongoWorkflow", username="root", password="example")
    db = client.workflows
    collection = db.workflow
    collection.insert_one(workflow_data.to_dict())
    return NoContent, 200


def delete_workflow(workflowId):  # noqa: E501
    """Delete a workflow from the database

     # noqa: E501

    :param workflowId: 
    :type workflowId: int

    :rtype: None
    """
    client = MongoClient("mongoWorkflow", username="root", password="example")
    db = client.workflows
    collection = db.workflow
    collection.delete_one({"id": workflowId})

    return NoContent, 200


def get_workflow(workflowId):  # noqa: E501
    """Retrieves a workflow from the database

     # noqa: E501

    :param workflowId: 
    :type workflowId: int

    :rtype: Workflow
    """
    client = MongoClient("mongoWorkflow", username="root", password="example")
    db = client.workflows
    collection = db.workflow
    result = collection.find_one({"id": workflowId}, {"_id": 0})
    if result is None:
        return NoContent, 404

    return result, 200


def get_workflows():  # noqa: E501
    """List all workflows in the system

     # noqa: E501


    :rtype: List[Workflow]
    """
    client = MongoClient("mongoWorkflow", username="root", password="example")
    db = client.workflows
    collection = db.workflow
    result = collection.find(projection={"_id": 0})
    if result is None:
        return NoContent, 404

    return list(result), 200
