import connexion
from connexion import NoContent
from pymongo import MongoClient

from swagger_server.models.submission import Submission  # noqa: E501


def add_submission(workflowId, formId, submission_data):  # noqa: E501
    """Add submission

     # noqa: E501

    :param workflowId: 
    :type workflowId: int
    :param formId: 
    :type formId: int
    :param submission_data: Submission that shall be added to the database
    :type submission_data: dict | bytes

    :rtype: None
    """
    if connexion.request.is_json:
        submission_data = Submission.from_dict(connexion.request.get_json())  # noqa: E501
    submission_data.form = formId
    client = MongoClient("mongoWorkflow", username="root", password="example")
    db = client.workflows
    collection = db.submission
    collection.insert_one(submission_data.to_dict())
    return NoContent, 200


def delete_submission(workflowId, formId, submissionId):  # noqa: E501
    """Delete submission

     # noqa: E501

    :param workflowId: 
    :type workflowId: int
    :param formId: 
    :type formId: int
    :param submissionId: 
    :type submissionId: int

    :rtype: None
    """
    client = MongoClient("mongoWorkflow", username="root", password="example")
    db = client.workflows
    collection = db.submission
    collection.delete_one({"id": submissionId})

    return NoContent, 200


def get_submission(workflowId, formId, submissionId):  # noqa: E501
    """Get submissions

     # noqa: E501

    :param workflowId: 
    :type workflowId: int
    :param formId: 
    :type formId: int
    :param submissionId: 
    :type submissionId: int

    :rtype: Submission
    """
    client = MongoClient("mongoWorkflow", username="root", password="example")
    db = client.workflows
    collection = db.submission
    result = collection.find_one({"id": submissionId}, {"_id": 0})
    if result is None:
        return NoContent, 404

    return result, 200


def get_submissions(workflowId, formId):  # noqa: E501
    """Get all submissions for a single form

     # noqa: E501

    :param workflowId: 
    :type workflowId: int
    :param formId: 
    :type formId: int

    :rtype: List[Submission]
    """
    client = MongoClient("mongoWorkflow", username="root", password="example")
    db = client.workflows
    collection = db.submission
    result = collection.find({"form": formId}, {"_id": 0})
    if result is None:
        return NoContent, 404

    return list(result), 200
