import connexion
import six
from connexion import NoContent
from pymongo import MongoClient

from swagger_server.models.form import Form  # noqa: E501
from swagger_server import util


def add_form(workflowId, form_data):  # noqa: E501
    """Insert a new form

     # noqa: E501

    :param workflowId: 
    :type workflowId: int
    :param form_data: From data to insert
    :type form_data: dict | bytes

    :rtype: None
    """
    if connexion.request.is_json:
        form_data = Form.from_dict(connexion.request.get_json())  # noqa: E501
    client = MongoClient("mongoWorkflow", username="root", password="example")
    db = client.workflows
    db.workflow.find_one_and_update(
        {"id": workflowId},
        {"$push": {"forms": form_data.to_dict()}}
    )
    return NoContent, 200


def delete_form(workflowId, formId):  # noqa: E501
    """Delete a form

     # noqa: E501

    :param workflowId: 
    :type workflowId: int
    :param formId: 
    :type formId: int

    :rtype: None
    """
    client = MongoClient("mongoWorkflow", username="root", password="example")
    db = client.workflows
    if db.workflow.find_one({"id": workflowId, "forms.id": formId}) is None:
        return NoContent, 404
    db.workflow.find_one_and_update(
        {"id": workflowId},
        {"$pull": {"forms": {"id": formId}}}
    )
    return NoContent, 200


def get_form(workflowId, formId):  # noqa: E501
    """Get a specific form from a workflow

     # noqa: E501

    :param workflowId: 
    :type workflowId: int
    :param formId: 
    :type formId: int

    :rtype: Form
    """
    client = MongoClient("mongoWorkflow", username="root", password="example")
    db = client.workflows
    res = db.workflow.find_one({"id": workflowId, "forms.id": formId}, {"_id": 0, "forms.$": 1})["forms"][0]
    if res is None:
        return NoContent, 404
    return res, 200


def get_forms(workflowId):  # noqa: E501
    """Get all forms to a workflow

     # noqa: E501

    :param workflowId: 
    :type workflowId: int

    :rtype: List[Form]
    """
    client = MongoClient("mongoWorkflow", username="root", password="example")
    db = client.workflows
    res = db.workflow.find_one({"id": workflowId}, {"_id": 0})["forms"]
    if res is None:
        return NoContent, 404
    return res, 200
